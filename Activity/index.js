
let trainer = {

    name: 'Ash Ketchum',
    age: 10,
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    talk: function () {
        console.log(this.pokemon[0] + " I choose you!")
    }
}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation");
console.log(trainer['pokemon']);
console.log("Result of talk method");
trainer.talk()

function Pokemon(pokemonName, level, health, attack) {

    this.pokemonName = pokemonName;
    this.level = level;
    this.health = health;
    this.attack = attack
    this.tackle = function (target) {
        console.log(this.pokemonName + " tackled " + target.pokemonName)
        target.health -= this.attack
        console.log(target.pokemonName + "'s" + " health is reduced to " + target.health)
    }
    this.faint = function (faintedTarget) {
        if (faintedTarget.health < 0) {
            console.log(faintedTarget.pokemonName + " fainted")
        }

    }
}

let pikachu = new Pokemon("Pikachu", 12, 24, 12)
console.log(pikachu)

let geodude = new Pokemon("Geodude", 8, 16, 8)
console.log(geodude)

let mewtwo = new Pokemon("Mewtwo", 100, 200, 100)
console.log(mewtwo)

geodude.tackle(pikachu)
console.log(pikachu)



mewtwo.tackle(geodude)

mewtwo.faint(geodude)

console.log(geodude)













